# -*- coding: utf-8 -*-
"""
Created on Aug   2018

@author: Chuang YU at ENSTA  alexchauncy@gmail.com
"""

# =============================================================================
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
np.random.seed(1234)

fs = 1e2
## load the database_chuang
gaitdata_1_DataFrame=pd.read_csv("D:\classification of emotion with gait data\HMM\gait_data_base_2400_56_after_medfilt.csv")
gaitdata_2=gaitdata_1_DataFrame.values[:,:]
gaitdata_3=gaitdata_2.reshape(300,8,56)
X=gaitdata_3
y=np.array([0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3])

X_8=X[12,:,:]

for i in np.arange(8):
    j=i
    X_in=X_8[j,:]
    f, Pxx_den = signal.welch(X_in, fs, noverlap=5,nperseg=28)
    plt.semilogy(f, Pxx_den)#plt.semilogy画多维数组
    plt.ylim([0.5e-6, 100])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()
    print(f.shape)
    print(f)
    print(Pxx_den.shape)
    print(Pxx_den)
#    print(f,Pxx_den)
