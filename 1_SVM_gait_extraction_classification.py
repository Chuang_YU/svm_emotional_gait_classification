# -*- coding: utf-8 -*-
"""
Created on Aug   2018
@author: Chuang YU at ENSTA  alexchauncy@gmail.com
"""

# =============================================================================
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn.svm import SVC

np.random.seed(1234)
#power spectral density
fs = 1e2
lable=["left_knee_angle","left_knee_angle_speed","right_knee_angle","right_knee_angle_speed","left_hip_angle","left_hip_angle_speed","right_hip_angle","right_hip_angle_speed"]

## load the database_chuang
gaitdata_1_DataFrame=pd.read_csv("D:\classification of emotion with gait data\HMM\gait_data_base_2400_56_after_medfilt.csv")
gaitdata_2=gaitdata_1_DataFrame.values[:,:]
gaitdata_3=gaitdata_2.reshape(300,8,56)
X=gaitdata_3
y=np.array([0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3])
###set the nperseg value in next " signal.welch(X_in, fs, noverlap=5,nperseg=28)"
nperseg_chuang=28
#initialize the narray to save the PSD
psd_seq_length=int(1+nperseg_chuang/2)#here is 15
gait_PSD=np.zeros((300,8,psd_seq_length))
gait_f=np.zeros((300,8,psd_seq_length))
#get the PSD
for i in np.arange(300):
    for j in np.arange(8):
        f,Pxx_chuang=signal.welch(X[i,j,:],fs,noverlap=5,nperseg=nperseg_chuang)#f:<class 'numpy.ndarray'> Pxx_chuang:
        gait_PSD[i,j,:]=Pxx_chuang
        gait_f[i,j,:]=f

gait_psd_svm_X=gait_PSD.reshape([300,8*psd_seq_length])#model input X
gait_psd_svm_y=y#model input y
#drawing the PSD 
#        plt.semilogy(f, Pxx_chuang)#plt.semilogy draw the multi-dimentions array
#        plt.ylim([0.5e-6, 100])
#        plt.xlabel('frequency [Hz]'+"  "+lable[j])
#        plt.ylabel('PSD [V**2/Hz]')
#        plt.show()
#        print(f,Pxx_chuang)
#split the test and train data
train_size_changable=280
X_train,X_test,y_train,y_test=train_test_split(gait_psd_svm_X, gait_psd_svm_y, train_size =train_size_changable, random_state=1, stratify=gait_psd_svm_y) 
# random_state=int means evert time get different data, shuttle=true means disorder them  
#stratify=y: input data 2:2:2 test data will be 1:1:1
#SVM model 
#__init__(C=1.0, kernel=’rbf’, degree=3, gamma=’auto’, coef0=0.0, shrinking=True, probability=False, tol=0.001, 
#         cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape=’ovr’, random_state=None)
#clf = SVC(decision_function_shape='ovo')
#train
clf = SVC()
clf.fit(X_train, y_train) 
#test
acc=clf.score(X_test,y_test)
print("acc:",acc)
 



 